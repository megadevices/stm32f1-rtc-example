//#include "stm32f10x.h"                  // Device header
#include "rtc.h"
#include <stdio.h>

static const uint8_t numofdays[] = {31,28,31,30,31,30,31,31,30,31,30,31};


void RTC_Init(void)
{	
	RTCTIME newtime;
	
	/* Enable PWR and BKP clocks */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR | RCC_APB1Periph_BKP, ENABLE);
	
  /* Allow access to BKP Domain */
  PWR_BackupAccessCmd(ENABLE);
	
	/* Backup data register value is not correct or not yet programmed
	(when the first time the program is executed) */
	if (BKP_ReadBackupRegister(BKP_DR1) != 0xA5A5)
  {
	  /* RTC Configuration */
    RTC_Configuration();	
		
    /* Adjust time by values entered by the user*/
		newtime.year = 2015;
		newtime.month  = 05;
		newtime.mday = 06;
		newtime.hour = 16;
		newtime.min = 30;
		newtime.sec = 00;
		RTC_SetTime(&newtime);
    BKP_WriteBackupRegister(BKP_DR1, 0xA5A5);
	}
	else 
	{
		RTC_WaitForSynchro();
	}
}

void RTC_Configuration(void)
{
  /* Reset Backup Domain */
  BKP_DeInit();

  /* Enable LSE */
  RCC_LSEConfig(RCC_LSE_ON);
  /* Wait till LSE is ready */
  while (RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET)
  {}

  /* Select LSE as RTC Clock Source */
  RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);

  /* Enable RTC Clock */
  RCC_RTCCLKCmd(ENABLE);

  /* Wait for RTC registers synchronization */
  RTC_WaitForSynchro();

  /* Wait until last write operation on RTC registers has finished */
  RTC_WaitForLastTask();

  /* Enable the RTC Second Interrupt */
  //RTC_ITConfig(RTC_IT_SEC, ENABLE);

  /* Wait until last write operation on RTC registers has finished */
  //RTC_WaitForLastTask();

  /* Set RTC prescaler: set RTC period to 1sec */
  RTC_SetPrescaler(32767); /* RTC period = RTCCLK/RTC_PR = (32.768 KHz)/(32767+1) */

  /* Wait until last write operation on RTC registers has finished */
  RTC_WaitForLastTask();
}


/*------------------------------------------*/
/* Convert time structure to timeticks      */
/*------------------------------------------*/

uint32_t RTC_GetTicksFromTime(uint8_t timetype, const RTCTIME *rtc)
{
	uint32_t utc, i, y;
	y = rtc->year - 1970;
	if (y > 2106 || !rtc->month || !rtc->mday) return 0;

	utc = y / 4 * 1461; y %= 4;
	utc += y * 365 + (y > 2 ? 1 : 0);
	for (i = 0; i < 12 && i + 1 < rtc->month; i++) {
		utc += numofdays[i];
		if (i == 1 && y == 2) utc++;
	}
	utc += rtc->mday - 1;
	utc *= 86400;
	if(timetype==time_midnight)
	{
		return utc;
	}
	else if(timetype==time_current)
	{
		utc += rtc->hour * 3600 + rtc->min * 60 + rtc->sec;
		return utc;
	}
	else return 0;
}


/*------------------------------------------*/
/* Get time ticks from RTC                  */
/*------------------------------------------*/
uint32_t RTC_GetTicksFromRTC(uint8_t timetype)
{
	uint32_t timeticks;
	timeticks = RTC_GetCounter();
  if(timetype==time_current)
	{
		return timeticks;
	}
	if(timetype==time_midnight)
	{
		timeticks /= 86400;
		timeticks *= 86400;
		return timeticks;
	}
	return 0;
}

/*------------------------------------------*/
/* Get time in calendar form                */
/*------------------------------------------*/

int RTC_GetTime(uint8_t timetype, RTCTIME *rtc)
{
	uint32_t n,i,d,utc;
	utc = RTC_GetCounter();
	/* Compute  hours */
	if(timetype==time_current)
	{
		rtc->sec = (uint8_t)(utc % 60); utc /= 60;
		rtc->min = (uint8_t)(utc % 60); utc /= 60;
		rtc->hour = (uint8_t)(utc % 24); utc /= 24;
	}
	if(timetype==time_midnight)
	{
		rtc->sec = 0;
		rtc->min = 0;
		rtc->hour = 0;
		utc/=86400;
	}
	rtc->wday = (uint8_t)((utc + 4) % 7);
	rtc->year = (uint16_t)(1970 + utc / 1461 * 4); utc %= 1461;
	n = ((utc >= 1096) ? utc - 1 : utc) / 365;
	rtc->year += n;
	utc -= n * 365 + (n > 2 ? 1 : 0);
	for (i = 0; i < 12; i++) {
		d = numofdays[i];
		if (i == 1 && n == 2) d++;
		if (utc < d) break;
		utc -= d;
	}
	rtc->month = (uint8_t)(1 + i);
	rtc->mday = (uint8_t)(1 + utc);
	return 1;
}

/*------------------------------------------*/
/* Set time in calendar form                */
/*------------------------------------------*/

int RTC_SetTime(const RTCTIME* rtc)
{
	uint32_t utc;
	utc = RTC_GetTicksFromTime(time_current, rtc);
	if (RTC_SetTimeFromTicks(utc))
	{
		return 1;
	}
	else return 0;
}

int RTC_SetTimeFromTicks(uint32_t ticks)
{
	/* Wait until last write operation on RTC registers has finished */
  RTC_WaitForLastTask();
  /* Change the current time */
  RTC_SetCounter(ticks);
  /* Wait until last write operation on RTC registers has finished */
  RTC_WaitForLastTask();
	return 1;
}	

uint32_t RTC_ClockHMtoTicks(char *s)
{
	uint8_t hours,mins;
	sscanf(s,"%u:%u",&hours,&mins);
	return hours*3600+mins*60;
}

uint32_t RTC_ClockMStoTicks(char *s)
{
	uint8_t mins,secs;
	sscanf(s,"%u:%u",&mins,&secs);
	return mins*60+secs;
}

void RTC_TicksToRTC (uint32_t ticks, RTCTIME* rtc)
{
	uint32_t n,i,d,utc;
	utc = ticks;
	/* Compute  hours */
		rtc->sec = (uint8_t)(utc % 60); utc /= 60;
		rtc->min = (uint8_t)(utc % 60); utc /= 60;
		rtc->hour = (uint8_t)(utc % 24); utc /= 24;
		rtc->wday = (uint8_t)((utc + 4) % 7);
		rtc->year = (uint16_t)(1970 + utc / 1461 * 4); utc %= 1461;
		n = ((utc >= 1096) ? utc - 1 : utc) / 365;
		rtc->year += n;
		utc -= n * 365 + (n > 2 ? 1 : 0);
		for (i = 0; i < 12; i++) {
			d = numofdays[i];
			if (i == 1 && n == 2) d++;
			if (utc < d) break;
			utc -= d;
		}
		rtc->month = (uint8_t)(1 + i);
		rtc->mday = (uint8_t)(1 + utc);
}

