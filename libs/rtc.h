#include "stm32f10x.h"                  // Device header

typedef struct {
	uint16_t	year;	/* 1970..2106 */
	uint8_t		month;	/* 1..12 */
	uint8_t		mday;	/* 1..31 */
	uint8_t		hour;	/* 0..23 */
	uint8_t		min;	/* 0..59 */
	uint8_t		sec;	/* 0..59 */
	uint8_t		wday;	/* 0..6 (Sun..Sat) */
} RTCTIME;


//���������� ��� �������, ������� ����� �������. ������� ��� ����� ������ �����.
#define time_current 1
#define time_midnight 2
#define time_oneday 86400

void RTC_Init(void);
void RTC_Configuration(void);

//�������� ����� �� RTC (������� ��� � �������)
int RTC_GetTime(uint8_t timetype, RTCTIME *rtc);

//���������� ����� � RTC
int RTC_SetTime(const RTCTIME *rtc);

//���������� ����� � �����
int RTC_SetTimeFromTicks(uint32_t ticks);

//������������� ����� �� RTC � ticks
uint32_t RTC_GetTicksFromTime(uint8_t timetype, const RTCTIME *rtc);

//�������� ����� � ����� (������� ��� � �������)
uint32_t RTC_GetTicksFromRTC(uint8_t timetype);

//������ ���� "12:10" "hh:mm" � ��������
uint32_t RTC_ClockHMtoTicks(char *s);

//������ ���� "02:10" "mm:ss" � ��������
uint32_t RTC_ClockMStoTicks(char *s);

//������������ ticks � ��������� RTCTIME
void RTC_TicksToRTC (uint32_t ticks, RTCTIME *rtc);

