#include "hardware.h" 
#include "rtc.h"
#include <stdio.h>

//����� ��� sprintf
char sprintf_buf[50];  

uint8_t mode=0;

/*
mode 0 - ������� ����� ������
mode 1 - ��������� �������
*/

char value[15]; //����� ��� �������� �������� � UART-� ��������
uint8_t b;//��������� �� value
uint32_t timeticks;

//��������� ������, �������� � USART1
void USART1_IRQHandler(void)
{
	char t; //���������� ��� ��������� ������.

	// check if the USART1 receive interrupt flag was set
	if( USART_GetITStatus(USART1, USART_IT_RXNE) )
	{
		t = USART1->DR; // the character from the USART1 data register is saved in t

		if(!mode)
		{
			switch (t)
			{
				case 'T':
				USART_Write(USART1,"Set Time\r\n");
				mode = 1;
						b=0;
						break;
			}
		}
		else 
		{
			switch (mode)
			{
				case 1:
				if(t !=0x0d && b<15)
				{
					value[b]=t;
					b++;
				}
				else
				{
					sscanf(value,"%d",&timeticks);
					RTC_SetTimeFromTicks(timeticks);
					mode=0;
				}
			}
		}
	}
}


void PrintClock (void)
{
	RTCTIME current_time;
	RTC_GetTime(time_current, &current_time);
	sprintf(sprintf_buf,"Date: %0.2d.%0.2d.%0.4d, Time: %0.2d:%0.2d:%0.2d\r\n", 
		current_time.mday,
		current_time.month,
		current_time.year,
		current_time.hour,
		current_time.min,
		current_time.sec);
	USART_Write(USART1, sprintf_buf);
}


int main (void)
{
	SystemInit();
	//configure SysTimer to 1 ms
	while(SysTick_Config(SystemCoreClock / 1000));
	
	USART1_Init();
	RTC_Init();
	USART_Write(USART1,"RTC Lib Example v.1.0. megadevices.com.ua\r");
	
	while(1)
	{
		if(!mode)
		{
			PrintClock();
			delay_ms(1000);
		}
	}
}