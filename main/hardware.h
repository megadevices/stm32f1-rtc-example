#include "stm32f10x.h"                  // Device header

/* Hardware defines */
#define USART1_BAUDRATE 115200

/* GPIO functions ---------------------------------  ------------------------*/
/*******************************************************************************
* Function Name  : GPIO_Config
* Description    : GPIO Configuration
* Input          : None
* Output         : None
* Return         : None
* Example        : GPIO_Config();
*******************************************************************************/


void USART_Transmit(USART_TypeDef *port, uint8_t c);

/*******************************************************************************
* Function Name  : USART_Write
* Description    : Print a zero terminated line to UART
* Input          : USART_TypeDefport - *port : USART port number
                   char - *s: Pointer to the first character
* Output         : None
* Return         : None
* Example        : USART_Write(USART1, "Hello");
*******************************************************************************/
void USART_Write (USART_TypeDef *port, char *s);


/*******************************************************************************
* Function Name  : USART1_Init
* Description    : USART1 Initialization
* Input          : None
* Output         : None
* Return         : None
* Example        : USART1_Init();
*******************************************************************************/
void USART1_Init(void);

/* TIMER functions ---------------------------------  ------------------------*/
/*******************************************************************************
* Function Name  : delay_ms
* Description    : Delay in ms
* Input          : uint32_t ms - delay in ms
* Output         : None
* Return         : None
* Example        : delay_ms(300);
*******************************************************************************/
void delay_ms(uint32_t ms);
