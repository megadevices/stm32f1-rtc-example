
#include "hardware.h"

void USART_Transmit(USART_TypeDef *port, uint8_t c)
{
	/* Write one byte in the USARTy Transmit Data Register */
	USART_SendData(port, c);
	/* Wait until end of transmit */
	while (USART_GetFlagStatus(port, USART_FLAG_TXE) == RESET);
}


void USART_Write (USART_TypeDef *port, char *s)
{
	while(*s)
	{
		USART_Transmit(port, *s++);
	}
}

void USART1_Init()
{
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	
	// Enable clock of modules
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_USART1, ENABLE);

  /* Configure the GPIO ports( USART1 Transmit and Receive Lines) */
  /* Configure the USART1_Tx as Alternate function Push-Pull */
  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  /* Configure the USART1_Rx as input floating */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10 ;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	// configure usart1
  USART_InitStructure.USART_BaudRate            = USART1_BAUDRATE;
  USART_InitStructure.USART_WordLength          = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits            = USART_StopBits_1;
  USART_InitStructure.USART_Parity              = USART_Parity_No;
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode                = USART_Mode_Rx | USART_Mode_Tx;
  USART_Init(USART1, &USART_InitStructure);

  /* Enable USART1 interrupt */
  USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
  //USART_ITConfig(USART1,USART_IT_TXE,ENABLE);

  /* Enable the USART1 */
  USART_Cmd(USART1, ENABLE);
  
	// configure usart1 interrupt
	//NVIC_SetVectorTable(NVIC_VectTab_FLASH, 0x0); 
	
  NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
}


//Timer Functions
unsigned int delay=0;

// Timer interrupt each 1 ms
void SysTick_Handler (void) 
{
	if (delay) delay--;
}

void delay_ms(uint32_t ms)
{
	delay=ms;
	while(delay){}
}